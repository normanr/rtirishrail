# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
import urllib
from xml.etree import ElementTree
from google.appengine.api import urlfetch


class Api(object):
  def __init__(self, base_url):
    self.base_url = base_url

  @staticmethod
  def element_list_to_dict_list(element_list):
    return [dict((subelement.tag.split('}')[-1], subelement.text)
            for subelement in element.getchildren())
            for element in element_list.getchildren()]

  @staticmethod
  def fetch_with_retry(url):
    for _ in xrange(3):
      try:
        return urlfetch.fetch(url, deadline=2)
      except urlfetch.DownloadError, download_error:
        logging.info('DownloadError: %s', download_error)
    raise urlfetch.DownloadError('Exceeded retry attempts')

  def __getattr__(self, name):
    def api_method(**kwargs):
      logging.info('Api.%s(%r)', name, kwargs)
      url = self.base_url + name + '?' + urllib.urlencode(kwargs).encode('utf8')
      response = self.fetch_with_retry(url)
      elements = ElementTree.fromstring(response.content)
      return self.element_list_to_dict_list(elements)
    return api_method
