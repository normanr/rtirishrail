var radiansPerDegree = Math.PI / 180;
var earthRadius = 6371;

function distanceBetweenPoints(latA, lonA, latB, lonB) {
  var lat1 = latA * radiansPerDegree, lon1 = lonA * radiansPerDegree;
  var lat2 = latB * radiansPerDegree, lon2 = lonB * radiansPerDegree;
  var dLat = lat2 - lat1;
  var dLon = lon2 - lon1;

  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(lat1) * Math.cos(lat2) *
          Math.sin(dLon/2) * Math.sin(dLon/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = earthRadius * c;
  return d;
}

function getClosestStation(lat, lon)
{
  stations = [];
  for (i in stationLocations) {
    station = stationLocations[i];
    stations[i] = [station[0], distanceBetweenPoints(station[1], station[2], lat, lon)];
  }
  stations.sort(function(a,b) { return a[1] - b[1]; });
  return stations[0];
}

function findClosestStation()
{
  geoResult = document.getElementById('geoResult');
  geoResult.innerHTML += '<li>Detecting closest station...</li>';
  doRedirect = document.referrer.slice(-4) != '/all';
  if (!navigator.geolocation) {
    geoResult.innerHTML += '<li>No location available.</li>';
    return;
  }
  var startTime = new Date().getTime();
  function success(position) {
    var endTime = new Date().getTime();
    var timeSpent = endTime - startTime;
    ll = position.coords;
    station = getClosestStation(ll.latitude, ll.longitude);
    geoResult.innerHTML +=
        '<li class="clickable" onclick="s(\'' + station[0] + '\')">' +
        station[0] + '<br/><span class="description">' +
        '~' + Math.round(station[1] * 100) * 10 + 'm away</div></li>';
    _gaq.push(
      ['_trackEvent', 'Closest', 'Found', station[0], 0, true],
      ['_trackTiming', 'Closest', 'Found', timeSpent, station[0]]);
    if (doRedirect && (station[1] < 10/*km*/)) {
      geoResult.innerHTML += '<li>Loading station...</li>';
      _gaq.push(function() { location.replace('/s/' + station[0]) });
    }
  }
  function error(error) {
    var endTime = new Date().getTime();
    var timeSpent = endTime - startTime;
    geoResult.innerHTML +=
        '<li>Error ' + error.code + ' occurred<br/>' +
        '<span class="description">' + error.message + '</span></li>';
    _gaq.push(
      ['_trackEvent', 'Closest', 'Error', error.message, error.code, true],
      ['_trackTiming', 'Closest', 'Error', timeSpent, error.code + ':' + error.message, 100]);
  }
  navigator.geolocation.getCurrentPosition(success, error, {'timeout':10000});
}
