# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


import functools
import logging
import os
import time
import urllib

from google.appengine.api import memcache
import webapp2
from django.template.loader import render_to_string


SERVER_SOFTWARE = os.environ.get('SERVER_SOFTWARE', '')
DEVELOPMENT_SERVER = SERVER_SOFTWARE.lower().startswith('devel')
NO_CACHE_MODE = DEVELOPMENT_SERVER


class BaseRequestHandler(webapp2.RequestHandler):
  best_time_templates = []
  forbidden_times = [None, '00:00']

  @property
  def api(self):
    return self.app.config.get('api')

  @classmethod
  def best_time(cls, details, substitution):
    for time_template in cls.best_time_templates:
      name = time_template % substitution
      if name in details and details[name] not in cls.forbidden_times:
        return details[name]
    return ''

  @staticmethod
  def template_values_or_cache(cache_key, cache_lifetime,
                               template_name, template_values_function,
                               *args, **kwargs):
    template_values = memcache.get(cache_key)
    if not template_values or NO_CACHE_MODE:
      try:
        template_values = template_values_function(*args, **kwargs)
        template_values['debug'] = 'cached at ' + time.strftime('%H:%M:%S')
        memcache.set(cache_key, template_values, cache_lifetime)
        template_values['debug'] = 'fresh, yummy!'
      except:
        logging.exception('Failed to render template')
        template_name = 'oops'
        template_values = {}
    return template_name, template_values

  def render_template(self, template_name, template_values):
    path = os.path.join(os.path.dirname(__file__),
                        'templates/' + template_name + '.html')
    self.response.out.write(render_to_string(path, template_values))

  def render_cache_or_template(self, cache_key, cache_lifetime,
                               template_name, template_values_function,
                               *args, **kwargs):
    template_name, template_values = self.template_values_or_cache(
        cache_key, cache_lifetime, template_name, template_values_function,
        *args, **kwargs)
    self.render_template(template_name, template_values)


class AllHandler(BaseRequestHandler):
  def get(self):
    template_name, template_values = self.template_values_or_cache(
        'all', 3600, 'allstations', self.get_all_stations)
    stations = template_values.get('stations')
    if stations:
      selected = [x for x in self.get_cookie_stations() if x in stations]
      template_values['stations'] = [{'name':s, 'checked':s in selected}
                                     for s in stations]
      self.response.set_cookie('stations',
          '|'.join([urllib.quote(x) for x in selected]), max_age=60 * 60 * 24 * 365)
    self.render_template(template_name, template_values)

  def get_cookie_stations(self):
    stations = self.request.cookies.get('stations')
    if not stations:
      return []
    return [urllib.unquote(s) for s in (stations or '').split('|')]

  def get_all_stations(self):
    stations = self.api.getAllStationsXML()
    stations = sorted(set(station['StationDesc'] for station in stations))
    return {'stations':stations}


class MainHandler(AllHandler):
  def get(self):
    stations = self.get_cookie_stations()
    if stations:
      template_name = 'index'
      template_values = {'stations': stations, 'debug':'favorite list'}
      self.response.set_cookie('stations',
          '|'.join([urllib.quote(x) for x in stations]), max_age=60 * 60 * 24 * 365)
    else:
      template_name, template_values = self.template_values_or_cache(
          'all', 3600, 'index', self.get_all_stations)
    self.render_template(template_name, template_values)


class ClosestHandler(AllHandler):
  def get(self):
    template_name, template_values = self.template_values_or_cache(
        'closest', 3600, 'closest', self.get_all_stations)
    self.render_template(template_name, template_values)

  def get_all_stations(self):
    stations = self.api.getAllStationsXML()
    return {'stations':stations}


class TrainHandler(BaseRequestHandler):
  best_time_templates = ['%s', 'Expected%s', 'Scheduled%s']

  def get(self, train):
    date = self.request.get('date')
    self.render_cache_or_template(
        'train/%s/%s' % (train, date), 10, 'train',
        self.get_train_schedule, train, date)

  def get_train_schedule(self, train, date):
    schedule = self.api.getTrainMovementsXML(TrainId=train, TrainDate=date)
    stations = []
    for row in schedule:
      if not row['LocationFullName']:
        row['LocationFullName'] = ''
      entrydict = {
        'station': '%(LocationFullName)s' % row,
        'arrival': self.best_time(row, 'Arrival'),
        'departure': self.best_time(row, 'Departure'),
        'LocationType': '%(LocationType)s' % row,
      }
      if row['StopType'] == 'C':
        bg_color = 'yellow'
      elif row['AutoArrival']:
        bg_color = 'green'
      else:
        bg_color = 'red'
      entrydict['bgcolor'] = bg_color
      for key in ('arrival', 'departure'):
        entrydict[key] = ':'.join(entrydict[key].split(':')[0:2])
      #logging.debug(entrydict)
      stations.append(entrydict)
    if schedule:
      row = schedule[0]
      origin_time = row['ScheduledDeparture'][:5]
      route = '%(TrainOrigin)s to %(TrainDestination)s' % row
      train_description = '%s - %s (%s)' % (origin_time, route, train)
    else:
      train_description = 'No schedule for %s on %s' % (train, date)
    return {
      'train': train_description,
      'stations' : stations}


class StationHandler(BaseRequestHandler):
  best_time_templates = ['%sarrival', '%sdepart']

  @staticmethod
  def sortable_time(time_str):
    try:
      hours, rest = time_str.split(':', 1)
      hours = int(hours)
      if hours < 3:
        hours += 24
      return '%02i:%s' % (hours, rest)
    except:
      logging.exception('Could not parse time: %s', time_str)
      return time_str

  def get(self, station):
    station = urllib.unquote(station)
    self.render_cache_or_template(
        'station/' + station, 10, 'station',
        self.get_station_schedule, station)

  def get_station_schedule(self, station):
    schedule = self.api.getStationDataByNameXML(StationDesc=station)
    if schedule:
      station = '%(Stationfullname)s at %(Querytime)s' % schedule[0]
    else:
      station = 'No schedule for %s' % station
    schedule_by_service_dict = {}
    for row in schedule:
      #logging.debug(row)
      row['Traincode'] = row['Traincode'].strip()
      row['Traindate'] = row['Traindate'].replace(' ','-')
      entrydict = {
        'service': '%(Direction)s %(Traintype)s' % row,
        'journey': '%(Traincode)s - %(Origin)s to %(Destination)s' % row,
        'sch': self.best_time(row, 'Sch'),
        'eta': self.best_time(row, 'Exp'),
        'due_in': row['Duein'] + 'm',
        'latest_information': row['Lastlocation'] or '',
        'Traincode': '%(Traincode)s' % row,
        'Traindate': '%(Traindate)s' % row,
      }
      #logging.debug(entrydict)
      service_name = entrydict['service']
      service = schedule_by_service_dict.get(service_name)
      if not service:
        service = schedule_by_service_dict[service_name] = []
      service.append(entrydict)
    schedule_by_service = [{
        'service':x,
        'schedule':sorted(schedule_by_service_dict[x],
                          key=lambda x: (self.sortable_time(x['eta']),
                                         self.sortable_time(x['sch'])))}
        for x in sorted(schedule_by_service_dict.keys())]
    return {
        'station': station,
        'schedule_by_service':schedule_by_service}

url_map = [('/', MainHandler),
           ('/all', AllHandler),
           ('/closest', ClosestHandler),
           ('/s/(.*)', StationHandler),
           ('/t/(.*)', TrainHandler)]
